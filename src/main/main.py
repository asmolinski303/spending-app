import sys
from typing import List
from expense.expense import Expense


def parse_expense_input(line: str) -> List:
    result = line.split(' ')
    if len(result) < 3:
        print("cannot parse line")
    elif len(result) > 3:
        print("cutting of the excess")
        return result[:3]
    else:
        return result

    return


def display_menu():
    info: str = '''Menu in spending app. Here are your options:
    1. add expense
    2. delete expense
    3. list expenses
    4. quit
give me the number'''
    print(info)
    while (True):
        choice = input()

        if choice == '1':
            print("adding expense")
            line = input()
            args = parse_expense_input(line)
            new_expense = Expense(args[0], args[1], args[2])
            print(new_expense)
        elif choice == '2':
            print("deleting expense")
        elif choice == '3':
            print("list expense")
        elif choice == '4':
            print("bye!")
            break


def parse_option():
    # should be used to recognize options given by user. Display is display
    pass


def main():
    # firstly lets add some menu: 1. add expense, list expenses, delete exense
    print(f"entry point")
    display_menu()


if __name__ == "__main__":
    sys.exit(main())
