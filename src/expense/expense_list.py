from .expense import Expense

class ExpenseList:
    
    def __init__(self):
        self.expenses = []

    def add_expense(self, expense: Expense):
        self.expenses.append(expense)
