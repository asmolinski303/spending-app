from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

setup(
    name='spending app',
    version='1.0.0',
    description='simple desc',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    entry_points={
        'console_scripts':[
            'spending_app=main:main',
            ],
        }
)
